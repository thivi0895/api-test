const express = require('express')
const app = express()
const port = 3000
const { ApolloServer, gql } = require("apollo-server")
const axios = require("axios")
const fs = require('firebase-admin')
const firebase = require('firebase')
const serviceAccount = require('./test-4562-firebase-adminsdk-xjdd7-05a00f3cb8.json')

/////////////////////////////////firebase///////////////////////////////
fs.initializeApp({
 credential: fs.credential.cert(serviceAccount)
});
const db = fs.firestore(); 

//////////////////////////////////////////////////////////////////////
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/patient/:id', getAppointments);
app.get('/patientList', getAllAppointment);
app.post('/appointment', postPatient);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

///////////////////////////////////get all appoinement////////////////////////////////////
async function getAllAppointment (req, res) {
  try {    
    // appointments
    const appointmentsSnapshot = await db.collection('users').get()

    if (!appointmentsSnapshot) {
      return res.status(400).send('Unable to get appointments info')
    } 

    let data = appointmentsSnapshot.docs.map(doc => doc.data())
    return res.status(200).send(data);
  } catch (e) {
    console.error(e)
    res.status(400).send('Unable to fulfill request.')
  }
}

/////////////////////////////get appointment by id////////////////////////////////
async function getAppointments (req, res) {
  try {    
  const appointmentsSnapshot = await db.collection('users').where("id", "==", parseInt(req.params.id)).get()

    if (!appointmentsSnapshot) {
      return res.status(400).send('Unable to get appointments info')
    }
    
    let data = appointmentsSnapshot.docs.map(doc => doc.data())
    
    return res.status(200).send(data)
  } catch (e) {
    console.error(e)
    res.status(400).send('Unable to fulfill request.')
  }
}

  ///////////////////create appoinement///////////////////////////////
 async function postPatient(req, res) {   

    let data = {
      name: req.query.name,
      email: req.query.email,
      phone: req.query.phone      
    };
    db.collection('appointment').add(data);

    console.log('created');
    return res.status(200).send(data);
  }

  ////////////////////////////graphql/////////////////////////////

const typeDefs = gql`
type Patient {
  id: Int
  first_name: String
  last_name: String
  email: String
  gender:String
  ip_address:String
}

type Query {
  patient: [Patient]
}

extend type Query {
  getPatient(
    id: Int 
  ): [Patient]
}
`
//////////////////////resolvers//////////////////////////////////
const resolvers = {
Query: {
  patient: async () => {  
    try {
      console.time('Execution Time');
      const patient = await axios.get("http://localhost:3000/patientList")      
      // const data = patient.data.map(({ id,first_name, email }) => ({
      //   id,
      //   first_name,
      //   email
      // }))
      console.timeEnd('Execution Time');
      return  patient.data
    } catch (error) {
      throw error
    }
  },
  getPatient: async (parent, args, context, info) => {  
    try {
      console.time('Execution Time');
      const patient = await axios.get("http://localhost:3000/patient/"+args.id) 
      console.timeEnd('Execution Time');
      return  patient.data
    } catch (error) {
      throw error
    }
  },
},
}

const server = new ApolloServer({
typeDefs,
resolvers,
})

server.listen().then(({ url }) => console.log(`Server ready at ${url}`))

/////////////////////////////////////////////////////////
